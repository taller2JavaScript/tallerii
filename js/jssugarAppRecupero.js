var abrirRecuperarContras = document.getElementById('abrirRecuperarContras'),
	overlayRecupero = document.getElementById('overlayRecupero'),
	popupRecupero = document.getElementById('popupRecupero'),
	btnCerrarPopupRecu = document.getElementById('btn-cerrar-popupRecu');

    abrirRecuperarContras.addEventListener('click', function(){
	overlayRecupero.classList.add('active');
	popupRecupero.classList.add('active');
});

btnCerrarPopupRecu.addEventListener('click', function(e){
	e.preventDefault();
	overlayRecupero.classList.remove('active');
	popupRecupero.classList.remove('active');
});